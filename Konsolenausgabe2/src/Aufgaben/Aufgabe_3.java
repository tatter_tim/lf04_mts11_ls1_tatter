package Aufgaben;

public class Aufgabe_3{
	
	public static void main(String[] args) {
		
		// Variablen Initialisierung
		
		String a = "Fahrenheit";
		String b = "Celsius";
		String c = "-----------------------";
		
		float c1 = -28.8889f;
		float c2 = -23.3333f;
		float c3 = -17.7778f;
		float c4 = -6.6667f;
		float c5 = -1.1111f;
		
		short f1 = -20;
		short f2 = -10;
		short f3 = 0;
		short f4 = 10;
		short f5 = 20;
		
		// Ausgabe der Tabelle
			
		System.out.printf ("%-12s" + "%s" + "%10s\n",a,"|",b);
		System.out.printf ("%s\n",c);
		System.out.printf ("%-12d" + "%s" + "%10.2f\n",f1,"|",c1);
		System.out.printf ("%-12d" + "%s" + "%10.2f\n",f2,"|",c2);
		System.out.printf ("%-12d" + "%s" + "%10.2f\n",f3,"|",c3);
		System.out.printf ("%+-12d" + "%s" + "%10.2f\n",f4,"|",c4);
		System.out.printf ("%+-12d" + "%s" + "%10.2f\n",f5,"|",c5);
	}
}