package Aufgaben;

public class Aufgabe_2{
	
	public static void main(String[] args) {
		
		String s = " 1 * 2 * 3 * 4 * 5";
			
		System.out.printf ("%s" + "%6s" + "%20s" + "%4s\n", "0!","=","=","1");
		System.out.printf ("%s" + "%6s" + "%-19.2s" + "%s" + "%4s\n", "1!","=",s,"=","1");
		System.out.printf ("%s" + "%6s" + "%-19.6s" + "%s" + "%4s\n", "2!","=",s,"=","2");
		System.out.printf ("%s" + "%6s" + "%-19.10s" + "%s" + "%4s\n", "3!","=",s,"=","6");
		System.out.printf ("%s" + "%6s" + "%-19.14s" + "%s" + "%4s\n", "4!","=",s,"=","24");
		System.out.printf ("%s" + "%6s" + "%-19s" + "%s" + "%4s\n", "5!","=",s,"=","120");
	}
}