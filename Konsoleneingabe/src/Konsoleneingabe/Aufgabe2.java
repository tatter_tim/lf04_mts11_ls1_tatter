package Konsoleneingabe;

import java.util.Scanner; // Import der Klasse Scanner

public class Aufgabe2
{
		public static void main(String[] args)
		{

			Scanner myScanner = new Scanner(System.in);
			
			System.out.print("Hallo Benutzer!\n");
			
			System.out.print("Wie lautet ihr Vorname?\n");
			String vorName = myScanner.next();
			
			System.out.print("Wie lautet ihr Nachname?\n");
			String nachName = myScanner.next();
			
			System.out.print("Wie alt sind sie?\n");
			int alter = myScanner.nextInt();

			System.out.printf("Ihr Name lautet: %s %s\n", vorName, nachName);
			System.out.printf("Ihr alter ist: %d Jahre", alter);

			myScanner.close();
		}
}