package Konsoleneingabe;

import java.util.Scanner; // Import der Klasse Scanner

public class Aufgabe1
{
		public static void main(String[] args)
		{

			Scanner myScanner = new Scanner(System.in);
			
			System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
			int zahl1 = myScanner.nextInt();
			
			System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
			int zahl2 = myScanner.nextInt();

			int ergebnis = zahl1 + zahl2;
			int ergebnis1 = zahl1 - zahl2;
			int ergebnis2 = zahl1 * zahl2;
			float ergebnis3 = (float)zahl1 / (float)zahl2;
			
			System.out.print("\n\n\nErgebnis der Addition lautet: ");
			System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);
			System.out.print("\n\n\nErgebnis der Subtraktion lautet: ");
			System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis1);
			System.out.print("\n\n\nErgebnis der Multiplikation lautet: ");
			System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis2);
			System.out.print("\n\n\nErgebnis der Division lautet: ");
			System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis3);
			
			myScanner.close();
		}
}