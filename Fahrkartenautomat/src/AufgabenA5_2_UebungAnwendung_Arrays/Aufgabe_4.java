package AufgabenA5_2_UebungAnwendung_Arrays;
import java.util.Scanner;

class Aufgabe_4{
	
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);
		System.out.print("Wie viele Zeilen wollen sie haben:");
		int zeilen = tastatur.nextInt();
		tastatur.close();
		double [] [] temperaturTabelle = new double [zeilen] [2];
		tabelleFuellenFahrenheit(zeilen, temperaturTabelle);
		tabelleFuellenCelsius(zeilen, temperaturTabelle);
		printTemperaturTabelle(temperaturTabelle);
	}
	
	public static void tabelleFuellenFahrenheit( int zeilen, double[] [] temperaturTabelle ) {
		double fahrenheit = 0;
		int zeile = 0;
		do {
			temperaturTabelle[zeile] [0] = fahrenheit;
			fahrenheit += 10;
			zeile++;
		} while ( zeile < zeilen);
	}
	
	public static void tabelleFuellenCelsius( int zeilen, double[] [] temperaturTabelle ) {
		double celsius = 0;
		double fahrenheit = 0;
		int zeile = 0;
		do {
			fahrenheit = temperaturTabelle[zeile] [0];
			celsius = berechneCelsius( fahrenheit );
			temperaturTabelle[zeile] [1] = celsius;
			zeile++;
		} while ( zeile < zeilen );
	}
	
	public static double berechneCelsius( double fahrenheit ) {
		double celsius = (5.0/9.0) * (fahrenheit - 32);
		return celsius;
	}
	
	public static void printTemperaturTabelle( double[] [] temperaturTabelle ) {
		for ( int index = 0; index < temperaturTabelle.length; index++) {
			System.out.printf("Fahrenheit: %.2f\tCelsius: %.2f\n", temperaturTabelle[index] [0], temperaturTabelle[index] [1]);
		}
	}
}
