package AufgabenA3_2_AB_Methoden;

public class Multiplikation {
	
    public static void main(String[] args) 
    {
    	double zahl1 = 2.36;
    	double zahl2 = 7.87;
    	
    	berechnung( zahl1, zahl2);
   	}
    
    public static void berechnung(double zahl1, double zahl2) 
    {
    	double ergebnis = zahl1 * zahl2;
    	System.out.println("Das Ergebnis lautet: " + ergebnis);
   	} 
}
