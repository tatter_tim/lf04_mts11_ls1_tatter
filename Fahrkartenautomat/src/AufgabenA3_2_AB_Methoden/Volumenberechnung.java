package AufgabenA3_2_AB_Methoden;

import java.util.Scanner;

public class Volumenberechnung {
	
    public static void main(String[] args) 
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double a;
    	double b;
    	double c;
    	double h;
    	double r;
    	double pi = 3.141592;
    	
    	System.out.println("Geben sie die Werte der Variablen an in der folgenden Reihenfolge a,b,c,h,r : ");
    	a = tastatur.nextDouble();
    	b = tastatur.nextDouble();
    	c = tastatur.nextDouble();
    	h = tastatur.nextDouble();
    	r = tastatur.nextDouble();
    	
    	berechnung ( a, b, c, h, r, pi );
   	}
    
    public static void berechnung(double a, double b, double c, double h, double r, double pi) 
    {
    	double wuerfel = a * a * a;
    	double quader = a * b * c;
    	double pyramide = a * a * ( h / (double) 3 );
    	double kugel = ( (double) 4 / (double) 3 ) * ( r * r * r ) * pi;
    			
    	System.out.printf("Das Volumen des Wuerfels V: " + "%.2f\n", wuerfel);
    	System.out.printf("Das Volumen des Quaders V: " + "%.2f\n", quader);
    	System.out.printf("Das Volumen der Pyramide V: " + "%.2f\n", pyramide);
    	System.out.printf("Das Volumen der Kugel V: " + "%.2f\n", kugel);
   	} 
}
