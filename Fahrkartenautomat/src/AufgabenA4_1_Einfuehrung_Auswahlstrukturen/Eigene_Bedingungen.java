package AufgabenA4_1_Einfuehrung_Auswahlstrukturen;
import java.util.Scanner;

class Eigene_Bedingungen
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.print("Bitte geben sie ihre zwei Zahlen ein: ");
    	double zahl1 = tastatur.nextDouble();
    	double zahl2 = tastatur.nextDouble();
    	
    	tastatur.close();
    	
    	if ( zahl1 == zahl2 )
    	{
    		System.out.print("Die erste Zahl enspricht der zweiten!\n");
    	}
    	
    	if ( zahl1 < zahl2 )
    	{
    		System.out.print("Die zweite Zahl ist groe�er wie die erste!\n");
    	}
    	
    	if ( zahl1 > zahl2 )
    	{
    		System.out.print("Die erste Zahl ist groe�er wie die zweite!\n");
    	}
    	else
    	{
    		System.out.print("Die erste Zahl ist nicht groe�er wie die zweite!\n");
    	}
    }
}