package AufgabenA4_1_Einfuehrung_Auswahlstrukturen;
import java.util.Scanner;

class Rabattsystem
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.print("Bitte geben sie den Bestellwert an: \n");
    	double bestellWert = tastatur.nextDouble();
    	
    	tastatur.close();
    	
    	if ( bestellWert <= 100 )
    	{
    		double ermaeßigterBestellwert = ( bestellWert / 100 ) * 109;
    		System.out.printf("Der ermäßigte Bestellwert lautet: " + "%.2f" + " Euro\n", ermaeßigterBestellwert );
    	} 
    	else if ( bestellWert < 500 )
    	{
    		double ermaeßigterBestellwert = ( bestellWert / 100 ) * 104;
    		System.out.printf("Der ermäßigte Bestellwert lautet: " + "%.2f" + " Euro\n", ermaeßigterBestellwert );
    	}
    	else
    	{
    		double ermaeßigterBestellwert = ( bestellWert / 100 ) * 99;
    		System.out.printf("Der ermäßigte Bestellwert lautet: " + "%.2f" + " Euro\n", ermaeßigterBestellwert );
    	}
    }
}