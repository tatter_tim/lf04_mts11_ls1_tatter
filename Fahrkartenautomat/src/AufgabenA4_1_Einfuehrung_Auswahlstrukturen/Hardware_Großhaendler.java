package AufgabenA4_1_Einfuehrung_Auswahlstrukturen;
import java.util.Scanner;

class Hardware_Gro�haendler
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.print("Bitte geben sie die Anzahl an Maeusen an: \n");
    	double anzahlMaeuse = tastatur.nextDouble();
    	
    	
    	System.out.print("Bitte geben sie den Einzelpreis einer Maus an: \n");
    	double preisProMaus = tastatur.nextDouble();
    	
    	tastatur.close();
    	
    	if ( anzahlMaeuse > 10 )
    	{
    		double gesamtPreis = ( anzahlMaeuse * preisProMaus ) + 10;
    		System.out.printf("Der Rechnungsbetrag lautet: " + "%.2f" + " Euro\n", gesamtPreis );
    	} 
    	else 
    	{
    		double gesamtPreis = anzahlMaeuse * preisProMaus;
    		System.out.printf("Der Rechnungsbetrag lautet: " + "%.2f" + " Euro\n", gesamtPreis );
    	}
    }
}