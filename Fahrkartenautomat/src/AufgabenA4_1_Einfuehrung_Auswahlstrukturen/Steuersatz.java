package AufgabenA4_1_Einfuehrung_Auswahlstrukturen;
import java.util.Scanner;

class Steuersatz
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.print("Bitte geben sie den Netto Betrag an: \n");
    	double nettoBetrag = tastatur.nextDouble();
    	
    	
    	System.out.print("Bitte geben sie nun mit 'j' den ermaeßigten Steuersatz oder mit 'n' den vollen an: \n");
    	char steuersatz = tastatur.next().charAt(0);
    	
    	tastatur.close();
    	
    	if ( steuersatz == 'j' )
    	{
    		double bruttoBetrag = ( nettoBetrag / 100 ) * 107;
    		System.out.printf("Der Brutto Betrag lautet: " + "%.2f" + " Euro\n", bruttoBetrag );
    	} 
    	else if ( steuersatz == 'n' )
    	{
    		double bruttoBetrag = ( nettoBetrag / 100 ) * 119;
    		System.out.printf("Der Brutto Betrag lautet: " + "%.2f" + " Euro\n", bruttoBetrag );
    	}
    	else
    	{
    		System.out.print("ungueltige Eingabe!");
    	}
    }
}