package AufgabenA4_1_Einfuehrung_Auswahlstrukturen;
import java.util.Scanner;

class BMI
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.print("Wie schwer sind sie in kg?: \n");
    	double gewicht = tastatur.nextDouble();
    	
    	System.out.print("Wie gro� sind sie in cm?: \n");
    	double groe�e = tastatur.nextDouble();
    	
    	System.out.print("Welches Geschlecht haben sie 'm' maennlich 'w' weiblich?: \n");
    	char geschlaecht = tastatur.next().charAt(0);
    	
    	tastatur.close();
    	
    	if ( geschlaecht == 'm' )
    	{
    		double bmi = gewicht / (( groe�e / 100 ) * groe�e );
    		klassifikationM( bmi );
    	} 
    	else if ( geschlaecht == 'w' )
    	{
    		double bmi = gewicht / (( groe�e / 100 ) * groe�e );
    		klassifikationW( bmi );
    	}
    	else
    	{
    		System.out.printf("Sollten sie ein 't' f�r transsexuell oder irgendeine andere ungueltige Eingabe getaetigt haben wird diese nicht unterst�tzt\n");
    	}
    }
    
    public static void klassifikationM( double bmi )
    {
    	if ( bmi < 20 )
    	{
    		System.out.print("Ihre BMI Klasse ist Untergewichtig\n");
    	}
    	else if ( bmi <= 25 )
    	{
    		System.out.print("Ihre BMI Klasse ist Normalgewichtig\n");
    	}
    	else if ( bmi > 25 )
    	{
    		System.out.print("Ihre BMI Klasse ist Uebergerwichtig\n");
    	}
    	else
    	{
    		System.out.print("ungueltiger BMI!\n");
    	}
    }
    
    public static void klassifikationW( double bmi )
    {
    	if ( bmi < 19 )
    	{
    		System.out.print("Ihre BMI Klasse ist Untergewichtig\n");
    	}
    	else if ( bmi <= 24 )
    	{
    		System.out.print("Ihre BMI Klasse ist Normalgewichtig\n");
    	}
    	else if ( bmi > 24 )
    	{
    		System.out.print("Ihre BMI Klasse ist Uebergerwichtig\n");
    	}
    	else
    	{
    		System.out.print("ungueltiger BMI!\n");
    	}
    }
}