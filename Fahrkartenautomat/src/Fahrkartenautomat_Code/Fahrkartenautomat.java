﻿package Fahrkartenautomat_Code;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    			
    	do
    	{
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen( tastatur );
    		double eingezahlterGesamtbetrag = fahrkartenBezahlen( zuZahlenderBetrag, tastatur );
    		fahrkartenAusgeben();
    		rueckgeldAusgeben( eingezahlterGesamtbetrag, zuZahlenderBetrag );
    	} while (true);
    }

    public static double fahrkartenbestellungErfassen( Scanner tastatur )
	{
    	boolean checkwhile = false;
    	boolean checkif = false;
    	double zuZahlenderBetrag = 0;
    	int ticketwahl;
    	int checkKaufen;
    	int anzahlTickets;
    	
    	double[] ticketpreise = { 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90 };
    	String[] beschreibung = { "Einzelfahrschein Berlin AB", 
    							 "Einzelfahrschein Berlin BC",
    							 "Einzelfahrschein Berlin ABC",
    							 "Kurzstrecke",
    							 "Tageskarte Berlin AB",
    							 "Tageskarte Berlin BC",
    							 "Tageskarte Berlin ABC",
    							 "Kleingruppen-Tageskarte Berlin AB",
    							 "Kleingruppen-Tageskarte Berlin BC",
    							 "Kleingruppen-Tageskarte Berlin ABC" };
    	
    	System.out.print("Fahrkartenbestellvorgang:\n");
    	System.out.print("=========================\n\n");
    	
    	do
    	{
    		System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
    		for ( int index = 0; index < 10; index++ ) {
    			System.out.printf((index+1) + ": %s Preis: %.2f\n", beschreibung[index], ticketpreise[index]);
    		}
    		System.out.print("Möchten sie ein (weiteres) Ticket kaufen? Ja (1), Nein (0)\n\n");
    		checkKaufen = tastatur.nextInt();
    		
    		if (checkKaufen == 1 ) {
    			System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n");
    			ticketwahl = tastatur.nextInt();
    		
    			if ( ticketwahl >= 1 && ticketwahl <= ticketpreise.length ) {
    				zuZahlenderBetrag += ticketpreise[ticketwahl-1];
    			} else {
    			System.out.print(">>Ungültige Ticketwahl bitte erneut eingeben!\n");
    			}
    		
    			if ( zuZahlenderBetrag > 0 && checkwhile == false)
    			{
    				do
    				{
    					System.out.print("Wählen sie bitte eine Anzahl von 1 bis 10 Tickets aus!\n");
    					System.out.print("Anzahl der Tickets: ");
    					anzahlTickets = tastatur.nextInt();
    		
    					if ( anzahlTickets > 10 || anzahlTickets < 1)
    					{
    						System.out.print(">>Ungültige Ticketanzahl bitte erneut eingeben!\n");
    					}
    					else
    					{
    						checkif = true;
    					}
    				} while (checkif == false);
    				zuZahlenderBetrag *= (double) anzahlTickets;
    				System.out.printf("Zwischensumme: " + "%.2f" + "\n\n", zuZahlenderBetrag);
    			}
    		} else if ( checkKaufen == 0 ){
    			System.out.print(">>Kaufvorgang abgeschlossen!\n\n");
    			checkwhile = true;
    		} else {
    			System.out.print(">>Ungueltige Auswahl bitte erneut eingeben!\n");
    		}
    	} while ( checkwhile != true );
    	return zuZahlenderBetrag;
	}
    
    public static double fahrkartenBezahlen( double zuZahlenderBetrag, Scanner tastatur ) 
	{
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMuenze;
    	do // Fußgesteuerte while schleife für das wiederholte Ausführen der Münzeingabe unter berücksichtigung der If Abfrag für die Geldstuecke
    	{
    		double nochZuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag; 		//Berechnung der Variable "nochZuZahlenderBetrag"
    		System.out.printf("Noch zu zahlen: " + "%.2f" + " EURO\n", nochZuZahlenderBetrag );	//Ausgabe der zuvor Berechneten Variable "nochZuZahlenderBetrag" mit maximal zwei Nachkommastellen
    		System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
    		eingeworfeneMuenze = tastatur.nextDouble();
    		if ( eingeworfeneMuenze == 2 || eingeworfeneMuenze == 1 || eingeworfeneMuenze == 0.5 || eingeworfeneMuenze == 0.2 || eingeworfeneMuenze == 0.1 || eingeworfeneMuenze == 0.05 )
    		{
    			eingezahlterGesamtbetrag += eingeworfeneMuenze;
    		}
    		else
    		{
    			System.out.printf("ungueltiger Muenzeinwurf!\n");
    		}
    	} while ( eingezahlterGesamtbetrag < zuZahlenderBetrag );
    	return eingezahlterGesamtbetrag;
	}
    
    public static void fahrkartenAusgeben() 
	{
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
           } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
           }
        }
        System.out.println("\n\n");
	}
    
    public static void rueckgeldAusgeben( double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) 
	{
    	double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO\n", rueckgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	  rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	  rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	  rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	  rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n");
	}
}

// Als Datentyp habe ich mich bei der Variablen "anzahlTickets" für einen Integer entschieden, da eine Ticketanzahl keine Kommastellen haben soll.
// Bei der Berechnung "anzahlTickets * ticketpreis" wird die Variable "anzahlTickets" in einen double gecastet um das multiplizieren mit "ticketpreis" möglich zu machen.
// Aufgabe A5.3.3: Vorteile: Preise und Bezeichnungen können in einer Datenstruktur vom Typ Array abgelegt und jederzeit verändert sowie leicht darauf zugegriffen werden durch den Index.
//				   Nachteile: Aufgrunddessen das der Index eines Arrays immer bei 0 anfängt muss immer nach der Benutzereingabe der richtige Index zum berechnen und ausgeben beachtet werden.