package AufgabenA4_4_Uebungen_Schleifen;

class einmaleins
{
    public static void main(String[] args)
    {
    	for (int i = 1; i <= 10; i++)
    	{
    		for (int j = 1; j <= 10; j++)
    		{
    			int ergebnis = i * j;
    			System.out.println( i + "x" + j + "=" + ergebnis );
    		}
    	}
    }
}

