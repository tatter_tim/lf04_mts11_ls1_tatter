﻿package AufgabenA4_5_Fahrkartenautomat_im_Endlosmodus;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
    	Scanner tastatur = new Scanner(System.in);
    	do
    	{
    		double zuZahlenderBetrag = fahrkartenbestellungErfassen( tastatur );
    		double eingezahlterGesamtbetrag = fahrkartenBezahlen( zuZahlenderBetrag, tastatur );
    		fahrkartenAusgeben();
    		rueckgeldAusgeben( eingezahlterGesamtbetrag, zuZahlenderBetrag );
    	} while (true);
    }

    public static double fahrkartenbestellungErfassen( Scanner tastatur ) 
	{
    	boolean check = false;
    	double ticketwahl;
    	double anzahlTickets;
    	
    	double einzelRegeltarif = 2.90;
    	double einzelTagesRegeltarif = 8.60;
    	double kleingruppenTagesRegeltarif = 23.50;
    	
    	System.out.print("Fahrkartenbestellvorgang:\n");
    	System.out.print("=========================\n\n");
    	
    	System.out.print("Mögliche Wunschfahrkarten für Berlin AB:\n");
    	System.out.print("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");
    	System.out.print("  Tageskarte Regeltarif AB [8,60 EUR] (2)\n");
    	System.out.print("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n\n");
    	
    	do
    	{
    		System.out.print("Welches Ticket möchten sie kaufen?: ");
    		ticketwahl = tastatur.nextDouble();
    		
    		if (ticketwahl%1 == 0) 
    		{
    			switch((int)ticketwahl)
    			{
    				case 1:
    					ticketwahl = einzelRegeltarif;
    					check = true;
    					break;
    				case 2:
    					ticketwahl = einzelTagesRegeltarif;
    					check = true;
    					break;
    				case 3:
    					ticketwahl = kleingruppenTagesRegeltarif;
    					check = true;
    					break;
    				default:
    					System.out.print("Ungültige Ticketwahl! Bitte erneut eingeben!\n");
    					break;
    			}
    		}
    		else
    		{
    			System.out.print("Ungültige Ticketwahl! Bitte erneut eingeben!\n");
    		}
    	}  while ( check != true );
    	
    	check = false;
    	
    	do
    	{
    		System.out.print("Anzahl der Tickets: ");
    		anzahlTickets = tastatur.nextDouble();
    		
    		if (anzahlTickets%1 == 0) 
    		{
    			check = true;
    		}
    		else
    		{
    			System.out.print("Ungültige Anzahl an Tickets! Bitte erneut eingeben!\n");
    		}
    	} while ( check != true );
        
        double zuZahlenderBetrag = ticketwahl * anzahlTickets;
        
        return zuZahlenderBetrag;
	}
    
    public static double fahrkartenBezahlen( double zuZahlenderBetrag, Scanner tastatur ) 
	{
    	double eingezahlterGesamtbetrag = 0.0;
    	double eingeworfeneMuenze;
    	double zwischenGesamtbetrag;
    	
    	do // Fußgesteuerte while schleife für das wiederholte Ausführen der Münzeingabe unter berücksichtigung der If Abfrag für die Geldstuecke
 	    {
 		   double nochZuZahlenderBetrag = zuZahlenderBetrag - eingezahlterGesamtbetrag; 		//Berechnung der Variable "nochZuZahlenderBetrag"
     	   System.out.printf("Noch zu zahlen: " + "%.2f" + " EURO\n", nochZuZahlenderBetrag );	//Ausgabe der zuvor Berechneten Variable "nochZuZahlenderBetrag" mit maximal zwei Nachkommastellen
 		   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
 		   eingeworfeneMuenze = tastatur.nextDouble();
 		   if ( eingeworfeneMuenze == 2 || eingeworfeneMuenze == 1 || eingeworfeneMuenze == 0.5 || eingeworfeneMuenze == 0.2 || eingeworfeneMuenze == 0.1 || eingeworfeneMuenze == 0.05 )
 		   {
 			   eingezahlterGesamtbetrag += eingeworfeneMuenze;
 		   }
 		   else
 		   {
 			   System.out.printf("ungueltiger Muenzeinwurf!\n");
 		   }
 	    } while ( eingezahlterGesamtbetrag < zuZahlenderBetrag );
    	return eingezahlterGesamtbetrag;
	}
    
    public static void fahrkartenAusgeben() 
	{
    	System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
           } catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
           }
        }
        System.out.println("\n\n");
	}
    
    public static void rueckgeldAusgeben( double eingezahlterGesamtbetrag, double zuZahlenderBetrag ) 
	{
    	double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        if(rueckgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " EURO\n", rueckgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
         	  rueckgabebetrag -= 2.0;
            }
            while(rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
         	  rueckgabebetrag -= 1.0;
            }
            while(rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
         	  rueckgabebetrag -= 0.5;
            }
            while(rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
         	  rueckgabebetrag -= 0.2;
            }
            while(rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
         	  rueckgabebetrag -= 0.1;
            }
            while(rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
         	  rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                           "vor Fahrtantritt entwerten zu lassen!\n"+
                           "Wir wünschen Ihnen eine gute Fahrt.\n\n");
	}
}

// Als Datentyp habe ich mich bei der Variablen "anzahlTickets" für einen Integer entschieden, da eine Ticketanzahl keine Kommastellen haben soll.
// Bei der Berechnung "anzahlTickets * ticketpreis" wird die Variable "anzahlTickets" in einen double gecastet um das multiplizieren mit "ticketpreis" möglich zu machen.